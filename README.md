#Dorothy - ActionEditor
&emsp;&emsp;这是一个使用Dorothy框架开发，用于配合Dorothy框架使用的骨骼动画编辑器。在这里提供二进制可执行版本的下载。详细使用说明见：[这里](http://www.luvfight.me/dorothy-action-editor-readme/)。  
&emsp;&emsp;编辑器在初次运行后可以在Win下的`C:\Users\Username\AppData\Local\Dorothy\Model`，或是在Mac下的`/Users/Username/Library/Caches/Model`路径中找到编辑器资源的输入输出目录。  
&emsp;&emsp;生成的动画模型在Dorothy框架中的使用方法，首先将模型文件包括jixienv.png，jixienv.clip和jixienv.model文件复制到同一个目录下，然后程序才能正确地加载所有文件。示例的加载代码如下：  
```lua
setfenv(Dorothy())

local winSize = CCDirector.winSize

local model = oModel("ActionEditor/Model/Output/jixienv.model") -- 加载动画模型
model.look = "happy" -- 设置外观  
model.loop = true -- 设置动画为循环播放  
model:play("walk") -- 播放动画  
model.position = oVec2(winSize.width*0.5,winSize.height*0.5)

local scene = CCScene()  
scene:addChild(model)

CCDirector:run(scene) 
```
&emsp;&emsp;该编辑器全部使用Dorothy框架的Lua接口提供的功能完成开发，所有的源码见`ActionEditor/Script`目录下的所有lua代码。了解Dorothy游戏框架：[这里](https://git.oschina.net/pig/Dorothy.git)。以下是部分编辑器运行的截图。  

![程序截图](http://git.oschina.net/pig/ActionEditor/raw/master/ActionEditor/screenshot1.PNG)
